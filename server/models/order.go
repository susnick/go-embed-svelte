package models

import "fmt"

type Order struct {
	ControlNumber string
	OrderNumber   string
	ClientID      int
}

func (o *Order) Save() {
	fmt.Println("Saving order ", o.ControlNumber)
}
